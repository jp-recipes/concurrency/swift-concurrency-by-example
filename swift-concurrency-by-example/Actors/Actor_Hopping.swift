//
//  Actor_Hopping.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import SwiftUI

// When a thread pauses work on one actor to start work on another actor instead we called actor hopping, and it will happen any time one actor calls another
//
// Swift manages a group of threads called `cooperative thread pool`, creating as many threads as there are CPU cores so that we can't be hit by thread explosion
// Actors guarantee that they can be running only on method at a time, but they don't care which thread they are running on
//
// Actor hopping with cooperative pool is fast - it will happen automatically, and we don't need to worry about it
// However the main thread is NOT part of the cooperative pool, which means actor code being run from the main actor will require a context switch, which will incur a performance penalty if donde too frequently
//
// We can see the problem caused by frequent actor hopping in this toy example
//
// For each number in the loop there will be a context switch between the main thread and another thread from the cooperative pool

private actor NumberProducer {
    var lastNumber = 1

    func getNext() -> Int {
        defer { lastNumber += 1 }
        return lastNumber
    }

    // This method run on the Main Actor
    @MainActor func run() async {
        for _ in 1...100 {
            // getNext() will run somewhere on the cooperative pool
            let nextNumber = await getNext()
            print("Loading \(nextNumber)")
        }
    }
}

private func callNumberProducer() async {
    let producer = NumberProducer()
    await producer.run()
}

// Another example from the real world

struct Entity: Identifiable {
    let id: Int
}

@MainActor
protocol DataModel: ObservableObject {
    var entities: [Entity] { get }

    func loadEntities() async
}

private struct ContentView<T: DataModel>: View {
    @StateObject var model: T

    var body: some View {
        List(model.entities) { entity in
            Text("Entity \(entity.id)")
        }
        .task {
            await model.loadEntities()
        }
    }
}

struct MutipleContextSwitches {
    // An actor that handles serial access to a database
    actor Database {
        func loadEntity(id: Int) -> Entity {
            Entity(id: id)
        }
    }

    // Implicitly @MainActor
    class Model: DataModel {
        @Published var entities = [Entity]()

        private let database = Database()

        func loadEntities() async { // Implicitly @MainActor
            for i in 1...100 {
                let entity = await database.loadEntity(id: i) // @MainActor <> Dtabase Actor
                entities.append(entity)
            }
        }
    }
}

struct SingleContextSwitch {
    actor Database {
        func loadEntities(ids: [Int]) -> [Entity] {
            ids.map { Entity(id: $0) }
        }
    }

    class Model: DataModel { // Implicitly @MainActor
        @Published var entities = [Entity]()

        private let database = Database()

        func loadEntities() async { // Implicitly @MainActor
            let ids = Array(1...100)

            // Load all entities in one hop
            let newEntities = await database.loadEntities(ids: ids)

            entities.append(contentsOf: newEntities)
        }
    }
}
