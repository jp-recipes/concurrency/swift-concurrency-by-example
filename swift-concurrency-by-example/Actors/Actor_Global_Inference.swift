//
//  Actor_Global_Inference.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation

// Apple explicitlty annotates many of its types as being @MainActor
// This includes most UIKit types such as UIView and UIButton
//
// However, there are may places where types gain main-actor-ness implicitly though a process called global actor inference
// Apple applies @MainActor automatically based on a set of predetermined rules
//
//
// There are five rules for global actor inference
//
// First, if a class is marker @MainActor, all its subclasses are also automatically @MainActor
//
// Second, if a method in a class is marked @MainActor, any overrides for that method are also automatically @MainActor
//
// Third, any struct or class using a property wrapper with @MainActor for its wrapped value will automatically be @MainActor
// This is what makes @StateObject and @ObservableObject convey main-actor-ness on SwiftUI views that use them - if we use any of them in a SwiftUI view, the whole view becomes @MainActor too
//
// Fourth, if a protocol declares a method as being @MainActor, any type that conforms to that protocol will have that same method automatically be @MainActor unless UNLESS we separate the conformance from the method


protocol DataStoring {
    @MainActor func save()
}

struct DataStore1 {
}

extension DataStore1: DataStoring { // Conformance and Definition
    func save() { }                 // Implicitlty @MainActor
}

struct DataStore2: DataStoring { // Conformance Only
}
extension DataStore2 {           // Declaration must be explicity
    @MainActor func save() { }
}

// Fifth, if the whole protocol is marked with @MainActor, any type that conforms to that protocol will also automatically be @MainActor UNLESS you put the conformance separatelt from the main type declaration, in which case only the methods are @MainActor

@MainActor protocol DataLoading {
    func load()
}

struct DataLoader1: DataLoading { // Conformance and Definition. struct is @MainActor
    func load() { }               // Implicitly @MainActor
}

struct DataLoader2: DataLoading { // Conformance Only. struct is @MainActor
}
extension DataLoader2 {
    func load() { }               // Implicitlty @MainActor
}

struct DataLoader3 {              // Non Conformance. struct is NOT @MainActor
}

extension DataLoader3: DataLoading {
    func load() { }               // This methods is implicitlty @MainActor
}
