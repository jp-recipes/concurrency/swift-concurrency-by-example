//
//  Actor_Create_URLCache.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation

// We avoid two problems
//
// 1. Attempting to read from a dictionary at the same time we're writing to it, which can cause our app to crash
// 2. Two or more simultaneous request for the same uncached URL coming in, forcing our code to fetch and store the same data repeatedly
//    This is a data race: whether two request or one dependes on the exact way our code is executed
//
// However, this esae of use doest come with somo extra responsibility
// * It's really important you keep in mind the serial queue behavior of actors, because is entirely possible we create a massive speed bumps in your code just because we wrote actor rather than class
//
// Think about the URL cache we hust made
// We forced it to load only a single URL at a time. If that's what we want then we'are all set
// If not, then we'll be wondering why all its requests are handled one by one

actor URLCache {
    private var cache = [URL: Data]()

    func data(for url: URL) async throws -> Data {
        if let cached = cache[url] {
            return cached
        }

        let (data, _) = try await URLSession.shared.data(from: url)
        cache[url] = data
        return data
    }
}

struct UsingActorApp {
    static func main() async throws {
        let cache = URLCache()

        let url = URL(string: "https://apple.com")!
        let apple = try await cache.data(for: url)

        let dataString = String(decoding: apple, as: UTF8.self)
        print(dataString)
    }
}
