//
//  Actor_NonIsolated_Codable.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation

actor CodableUser: Codable {
    enum CodingKeys: CodingKey {
        case username, password
    }

    let username: String
    let password: String

    var isOnline: Bool = false

    init(username: String, password: String, isOnline: Bool = false) {
        self.username = username
        self.password = password
        self.isOnline = isOnline
    }

    nonisolated func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(username, forKey: .username)
        try container.encode(password, forKey: .password)
    }
}

private func useNonisolatedCodable() {
    let user = CodableUser(username: "twostraws", password: "123456")

    if let encoded = try? JSONEncoder().encode(user) {
        let json = String(decoding: encoded, as: UTF8.self)
        print(json)
    }
}
