//
//  MainActor_Run_MainActor.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation

// IMPORTANT
// If our functions is already running on the main actor, using `await MainActor.run()` will run our code immediately without waiting for the next run loop, but using Task will wait for the next run loop

@MainActor class MainActorViewModel: ObservableObject {
    func runTest() async { // Current run loop
        print("1")

        await MainActor.run {
            print("2")

            Task { @MainActor in // Queued for the next run loop
                print("3")
            }

            print("4")
        }

        print("5")
    }
}

private func callRunTest() async {
    let viewModel = await MainActorViewModel()
    await viewModel.runTest()

    // 1
    // 2
    // 4
    // 5
}
