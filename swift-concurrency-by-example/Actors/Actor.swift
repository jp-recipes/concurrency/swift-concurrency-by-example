//
//  Actor.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation

// Swift's actors are conceptually like classes that are safe to us in concurrent environments
// This safety is made possible because Swift automatically ensures no two pieces of code attempt to access an actor's data at the same time - it is made impossible by the compiler, rather than requiring developers to write boilerplate code using systems such as locks
//
// 1. Actors are created using the `actor` keyword
// 2. Like classes, actors are reference types. This makes them useful for sharing state
// 3. They have many of the same features as classes
// 4. Actors do not support inheritance, so the cannot have convenience initializers and don not support either final or override
// 5. All actors automatically conform the Actor protocol, which no other type can use
//    This allows you to write code restricted to work only with actors
//
// If we are attempting to read a variable property or call a method on an actor, and you0re doing it from outside the actor itlsef, you must do so synchronouslt using await

// 1. The Scorer type is created with the actor keyword
actor Scorer {
// 2. It can have properties and methods
    var score = 10

    func printScore() {
// 3. The internal method can access the local score property
        print("My score is \(score)")
    }
// 4. We can not access another actor property without using await keyword
    func copyScore(from other: Scorer) async {
        score = await other.score
    }
}

private func useActors() async {
    let actor1 = Scorer()
    let actor2 = Scorer()
// 5. Code outside the actor also needs to use await
    await print(actor1.score)
    await actor1.copyScore(from: actor2)
}

// This means several things
//
// 1. Actors are effectively operating a provate serial queue for their messaging inbox, taking request one at a time and fulfilling them
//    This executes requests in the order they were received, but we can also use task priority to escalate requests
// 2. Only one piece of code at a time can access an actor's mutable state unless we make some things as being unprotected. Swift calls this `actor isolation`
// 3. Just like regular await calls, reading an actor's property or method marks a potential suspention point
// 4. Any state that is NOT mutable (constants) can be accessed without await, because it's always going to be safe
//
// NOTE
// Writing properties from outside an actor is not allowed
//
// When to use actors ?
// * If we ever need to make sure that acces to some object is restricted to a single task a time, actors are perfect
// * There are also times when simultaneous concurrent access to data can cause `data races` - when the outcome of your case depends on the order in which tasks complete. These errors are particulary nasty to find and fix, but with actors such a data races become impossible
//

