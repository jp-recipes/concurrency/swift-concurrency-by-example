//
//  Actor_Isolated_Parameters.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation

// Any properties and methods that belong to an actor are isolated to that actor, but you can make external functions isolated to an actor
//
// The debugLog() method prints the actor properties without using await
// What makes this possible is the use of the isolated keyword in the function signature
//
// Using isolated like thios does NOT bypass any of the underlying safety or implementation of actors
// There can still only be one thread accessing the actor at any time
// What we've done just pushes the that access out by a level,because now the whole function must be run on that actor rather than just individual lines inside it
// This means debugLog(dataStore:) needs to be called using await even though it isn't marked as async
//
// IMPORTANT
// This makes the function itself a singlet potential suspention point rather than individual accesses to the actor being suspension points

actor DataStore {
    var username = "Anonymous"
    var friends = [String]()
    var highScores = [Int]()
    var favorites = Set<Int>()

    init() {
        // load data here
    }

    func save() {
        // save data here
    }
}

private func debugLog(dataStore: isolated DataStore) {
    print("Username: \(dataStore.username)")
    print("Friends: \(dataStore.friends)")
    print("High scores: \(dataStore.highScores)")
    print("Favorites: \(dataStore.favorites)")
}

private func useIsolatedDebugLog() async {
    let data = DataStore()
    await debugLog(dataStore: data)
}
