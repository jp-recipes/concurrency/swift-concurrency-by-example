//
//  Actor_Safe_BankAccount.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation

actor BankAccount {
    var balance: Decimal

    init(initialBalance: Decimal) {
        self.balance = initialBalance
    }

    private func deposit(amount: Decimal) {
        balance = balance + amount
    }

    func transfer(amount: Decimal, to other: BankAccount) async {
        // Check that we have enough money to pay
        guard balance > amount else { return }

        // Substract it from our balance
        balance = balance - amount

        // Send it to other account
        await other.deposit(amount: amount)
    }
}

func useSafeBankAccount() async {
    let firstAccount = BankAccount(initialBalance: 500)
    let secondAccount = BankAccount(initialBalance: 0)

    await firstAccount.transfer(amount: 500, to: secondAccount)
}
