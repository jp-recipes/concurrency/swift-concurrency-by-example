//
//  MainActor.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation

// @MainActor is a global actor that uses the main queue for executing its work
// In practice, this means methods or types with @MainActor can (cor the most part) safely modify the UI because it will always be running on the main queue, and calling MainActor.run() will push some custom work of our choosing the main actor, and thus the main queue

// We could create an observable object que two @Published properties, and because they will both update the UI we would mark the whole class with @MainActor to ensure these UI updates always happen on the main actor

// @MainActor makes a single method or all methods on a type run on the main actor
// This is particulary useful for any types that exist to update our UI, such as ObservableObject classes

@MainActor
class AccountViewModel: ObservableObject {
    @Published var username = "Anonymous"
    @Published var isAuthenticated = false
}

// This set up is so central to the way ObservableObject works that SwiftUI bakes it right in
// Whenever we use @StateObject or @ObservableObject inside a view, Swift will ensure that the whole view runs on the main actor so that you can't accidentally try to publish UI updates in a dangerous way
//
// Even better, no matter what property wrappers we use, the `body` property of our SwiftUI views is always run on the main actor
//
// Does that mean we don't need to explicitly add @MainActor to observable objects ?
// Well, no - there is still benefits to using @MainActor with these clasess
//
//
// RECOMMENDATION
// Even though SwiftUI ensures main-actor-ness when using @ObservableObject, @StateObject, and SwiftUI view body properties, it's a good idea to add the @MainActor attribute to all our observable object classes to be absolutely sure all UI updates happen on the main actor
//
//  If we need certain methods or computed properties to opt out of running on the main actor, use nonisolated as you would do with a regular actor

// IMPORTANT
// Any typa that has @MainActor objects as properties will also implicitly be @MainActor using global actor inference - a set of rules that Swift applies to make sure global-actor-ness works without getting in the way too much

// The magic of @MainActor is that it automatically forces methods or whole types to run on the main actor
// Previously we needed to do it by hand with code like DispatchQueue.main.async()



















