//
//  MainActor_Run.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation

// Be careful: @MainActor is really helpful to make code run on the main actor, but it's not foolproof
// For example:
//   A main actor method could trigger code to run on a background task
//   If we're using Face ID and call evaluatepolicy() to authenticate the use, the completion handler will be called on a background thread even though the code is still within the @MainActor class
//
// If we do need to spontaneosuly run some code on the main actor, we can do that calling MainActor.run()

func couldBeAnywhere() async {
    await MainActor.run {
        print("This is on the main actor.")
    }

    let result = await MainActor.run { () -> Int in
        print("This is on the main actor.")
        return 42
    }

    print(result)

    Task { @MainActor in
        print("This is on the main actor.")
    }
}

