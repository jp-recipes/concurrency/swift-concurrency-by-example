//
//  Actor_NonIsolated.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation
import CryptoKit

// All methods and mutable properties inside an actor are isolated to that actor by default, which means they cannot be accessed directly from code that's external to the actor
//
// Access to constant properties is automatically allowed because they are inherently safe from race conditions
//
// If we want we can make some method excepted by using the nonisolated keyword
//
// Actors methods that are non-isolated can access other non-isolated state, such as constant properties or other methods that are marked as non-isolated
// However, they can NOT directly access isolated state like an isolated actor method would; the need to use await instead

actor CryptoUser {
    let username: String
    let password: String
    var isOnline: Bool

    init(username: String, password: String, isOnline: Bool = false) {
        self.username = username
        self.password = password
        self.isOnline = isOnline
    }

    nonisolated func passwordhash() -> String {
        let passwordData = Data(password.utf8)
        let hash = SHA256.hash(data: passwordData)
        return hash.compactMap {
            String(format: "%02x", $0)
        }.joined()
    }
}

// No need for the use of async await
private func useNonIsolatedMembers() {
    let user = CryptoUser(username: "twostraws", password: "123456")
    print(user.passwordhash())
}


