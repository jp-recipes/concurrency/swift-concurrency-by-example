//
//  Actor_BankAccount.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation

// Unsafe concurrent operations
//
// In the worst case two parallel calls to transfer() would be called on the same UnsafeBankAccount instance, and the following would occur
//
// 1. The first would check whether the balance was sifficient for the transfer. It is, so the code would continue
// 2. The second would also check whether the balance was sufficient for the transfer. It is, so the code would continue
// 3. The first would then substract the amount from the balance, and deposit it in the other account
// 4. The second would then substract the amount from the balance, and deposit it in the other account
//
// If we switch this type to be an actor, that problems goes away

class UnsafeBankAccount {
    var balance: Decimal

    init(initialBalance: Decimal) {
        self.balance = initialBalance
    }

    func deposit(amount: Decimal) {
        balance = balance + amount
    }

    // Data race
    func transfer(amount: Decimal, to other: UnsafeBankAccount) {
        // Check that we have enough money to pay
        guard balance > amount else { return }

        // Substract it from our balance
        balance = balance - amount

        // Send it to other account
        other.deposit(amount: amount)
    }
}

func useUnsafeBankAccount() {
    let firstAccount = UnsafeBankAccount(initialBalance: 500)
    let secondAccount = UnsafeBankAccount(initialBalance: 0)

    firstAccount.transfer(amount: 500, to: secondAccount)
}
