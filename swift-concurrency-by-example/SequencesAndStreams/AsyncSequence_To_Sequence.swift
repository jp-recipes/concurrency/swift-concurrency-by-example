//
//  AsyncSequence_To_Sequence.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import Foundation

extension AsyncSequence {
    func collect() async rethrows -> [Element] {
        try await reduce(into: [], { $0.append($1) })
    }
}

func getNumberArray() async throws -> [Int] {
    let url = URL(string: "https://hws.dev/random-numbers.txt")!
    let numbers = url.lines.compactMap(Int.init)
    return try await numbers.collect()
}
