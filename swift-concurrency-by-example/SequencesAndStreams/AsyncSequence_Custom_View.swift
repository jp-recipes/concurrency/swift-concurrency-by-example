//
//  AsyncSequence_Custom_View.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import SwiftUI

struct AsyncSequence_Custom_View: View {
    @State private var users = [User]()

    var body: some View {
        List(users) { user in
            Text(user.name)
        }
        .task {
            await fetchUsers()
        }
    }

    func fetchUsers() async {
        let url = URL(fileURLWithPath: "PATH")
        let urlWatcher = URLWatcher(url: url, delay: 3)

        do {
            for try await data in urlWatcher {
                try withAnimation {
                    users = try JSONDecoder().decode([User].self, from: data)
                }
            }
        } catch {
            // just bail out
        }
    }
}

struct AsyncSequence_Custom_View_Previews: PreviewProvider {
    static var previews: some View {
        AsyncSequence_Custom_View()
    }
}
