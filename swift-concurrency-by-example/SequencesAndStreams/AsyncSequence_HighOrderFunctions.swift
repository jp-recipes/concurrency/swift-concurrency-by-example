//
//  AsyncSequence_HighOrderFunctions.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import Foundation

struct Quote {
    let text: String
}

// Map

func showQuotes() async throws {
    let url = URL(string: "https://hws.dev/quotes.txt")!
    let uppercasesLines = url.lines.map(\.localizedLowercase)

    for try await line in uppercasesLines {
        print(line)
    }
}

func printQuotes() async throws {
    let url = URL(string: "https://hws.dev/quotes.csv")!

    let quotes = url.lines.map(Quote.init)

    for try await quote in quotes {
        print(quote.text)
    }
}

// Filter

func printAnonymousQuotes() async throws {
    let url = URL(string: "https://hws.dev/quotes.txt")!

    let anonymousQuotes = url.lines.filter {
        $0.contains("Anonymous")
    }

    for try await line in anonymousQuotes {
        print(line)
    }
}

// Prefix

func printTopQuotes() async throws {
    let url = URL(string: "https://hws.dev/quotes.txt")!
    let topQuotes = url.lines.prefix(5)

    for try await line in topQuotes {
        print(line)
    }
}


// Returning a Sequence

func getQuotes() async -> some AsyncSequence {
    let url = URL(string: "https://hws.dev/quotes.txt")!

    return url.lines
        .filter { $0.contains("Anonymous") }
        .prefix(5)
        .map(\.localizedLowercase)
}


// AllSatisfy (single result). Use it with finite sequences

func checkQuotes() async throws {
    let url = URL(string: "https://hws.dev/quotes.txt")!
    let noShortQuotes = try await url.lines
        .allSatisfy { $0.count > 30 }

    print(noShortQuotes)
}

// Max

func printHighestNumber() async throws {
    let url = URL(string: "https://hws.dev/quotes.txt")!

    if let highest = try await url.lines.compactMap(Int.init).max() {
        print("Highest number: \(highest).")
    } else {
        print("No number was the highest.")
    }
}


// Reduce

func sumRandomNumbers() async throws {
    let url = URL(string: "https://hws.dev/quotes.txt")!

    let sum = try await url.lines.compactMap(Int.init).reduce(0, +)

    print("Sum of numbers: \(sum)")
}
