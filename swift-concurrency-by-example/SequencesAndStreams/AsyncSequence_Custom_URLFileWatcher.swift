//
//  AsyncSequence_Custom.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import Foundation

// 1. Conform to the AsyncSequence and AsyncSequenceProtocol
// 2. The next() method must be marker `async`
// 3. Crea a method makeAsyncIterator()

// Simple AsyncSequence

struct DoubleGenerator: AsyncSequence {
    typealias Element = Int

    struct AsyncIterator: AsyncIteratorProtocol {
        var current = 1

        mutating func next() async -> Element? {
            defer { current &*= 2 }

            if current < 0 { return nil }

            return current
        }
    }

    func makeAsyncIterator() -> AsyncIterator {
        AsyncIterator()
    }

}

func callDoubleGenerator() async throws {
    let sequence = DoubleGenerator()

    for await number in sequence {
        print(number)
    }
}


// More Complex AsyncSequence

struct URLWatcher: AsyncSequence {
    typealias Element = Data

    let url: URL
    let delay: Int

    init(url: URL, delay: Int = 10) {
        self.url = url
        self.delay = delay
    }

    func makeAsyncIterator() -> AsyncIterator {
        AsyncIterator(url: url, delay: delay)
    }

    struct AsyncIterator: AsyncIteratorProtocol {
        let url: URL
        let delay: Int
        private var comparisonData: Data?
        private var isActive = true

        init(url: URL, delay: Int) {
            self.url = url
            self.delay = delay
        }

        mutating func next() async throws -> Element? {
            // Once we're inactive always return nil immediately
            guard isActive else { return nil }

            if comparisonData == nil {
                // If this is our first iteration, return the initial value
                comparisonData = try await fetchData()
            } else {
                while true {
                    try await Task.sleep(nanoseconds: UInt64(delay) * 1_000_000_000)

                    let latestData = try await fetchData()

                    if latestData != comparisonData {
                        // New data is different from previous data,
                        // so update previous data and send it back
                        comparisonData = latestData
                        break
                    }
                }
            }

            if comparisonData == nil {
                isActive = false
                return nil
            }

            return comparisonData
        }

        private func fetchData() async throws -> Element {
            let (data, _) = try await URLSession.shared.data(from: url)
            return data
        }
    }
}
