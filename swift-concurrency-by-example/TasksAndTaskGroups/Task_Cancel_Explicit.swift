//
//  Task_Cancel_Validation.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

// We can use both Task.checkCancellation() and Task.isCancelled() from both synchronous and synchronous functions
// Remember, async functions can call synchronous functions freely, so checking for cancellation can be just as important to avoid doing unnecessary work

func getAverageTemperatureIsCancelled() async {
    let fetchTask = Task { () -> Double in
        let url = URL(string: "https://hws.dev/readings.json")!
        do {
            let (data, _) = try await URLSession.shared.data(from: url)

            if Task.isCancelled {
                return 0
            }

            let readings = try JSONDecoder().decode([Double].self, from: data)
            let sum = readings.reduce(0, +)
            return sum / Double(readings.count)
        } catch {
            return 0
        }
    }

    fetchTask.cancel()

    let result = await fetchTask.value
    print("Average temperature: \(result)")
}
