//
//  TaskGroup_Cancel_View.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import SwiftUI

struct TaskGroup_Cancel_View: View {
    @State private var stories = [NewsStory]()

    var body: some View {
        NavigationView {
            List(stories) { story in
                VStack(alignment: .leading) {
                    Text(story.title)
                        .font(.headline)

                    Text(story.strap)
                }
            }
            .navigationTitle("Latest News")
        }
        .task {
            await loadStories()
        }
    }

    // This code attempts to fetch, merge, and display the contents of five news feeds
    // If any of the fetches throws an error, the whole group will throw an error and end
    //
    // If a fetch somehow succeeds while ending up with an empty array it menas our data quota has run out and we shold stop trying any other feed fetches
    private func loadStories() async {
        do {
            try await withThrowingTaskGroup(of: [NewsStory].self) { group -> Void in
                for i in 1...5 {
                    group.addTask {
                        let url = URL(string: "https://hws.dev/news-\(i).json")!
                        let (data, _) = try await URLSession.shared.data(from: url)

                        try Task.checkCancellation()

                        return try JSONDecoder().decode([NewsStory].self, from: data)
                    }
                }

                for try await result in group {
                    if result.isEmpty {
                        group.cancelAll()
                    } else {
                        stories.append(contentsOf: result)
                    }
                }

                stories.sort { $0.id < $1.id }
            }
        } catch {
            print("Failed to load stories: \(error.localizedDescription)")
        }
    }
}

struct TaskGroup_Cancel_View_Previews: PreviewProvider {
    static var previews: some View {
        TaskGroup_Cancel_View()
    }
}
