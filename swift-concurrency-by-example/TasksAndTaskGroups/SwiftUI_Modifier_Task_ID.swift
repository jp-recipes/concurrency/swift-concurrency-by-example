//
//  SwiftUI_Task_ID.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import SwiftUI

struct SwiftUI_Task_ID: View {
    @State private var messages = [Inbox]()
    @State private var selectedBox = "Inbox"

    let messageBoxes = ["Inbox", "Sent"]

    var body: some View {
        NavigationView {
            List {
                Section {
                    ForEach(messages) { message in
                        VStack(alignment: .leading) {
                            Text(message.user)
                                .font(.headline)

                            Text(message.text)
                        }
                    }
                }
            }
            .listStyle(.insetGrouped)
            .navigationTitle(selectedBox)
            // The task modifier will recreate its fetchData() task whenever selectedBox changes
            .task(id: selectedBox) {
                await fetchData()
            }
            .toolbar {
                Picker("Select a message box", selection: $selectedBox) {
                    ForEach(messageBoxes, id: \.self, content: Text.init)
                }
                .pickerStyle(.segmented)
            }
        }
    }

    private func fetchData() async {
        do {
            let url = URL(string: "https://hws.dev/\(selectedBox.lowercased()).json")!
            let (data, _) = try await URLSession.shared.data(from: url)
            messages = try JSONDecoder().decode([Inbox].self, from: data)
        } catch {
            messages = [
                Inbox(id: 0, user: "Failed to load message box.", text: "Please try again later.")
            ]
        }
    }
}

struct SwiftUI_Task_ID_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUI_Task_ID()
    }
}
