//
//  Task_Cancel.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

// We can tell a task to stop wok, but the task is free to completely ignore that instruction
//
// 1. We can call the cancel() method
// 2. Any task can check Task.isCancelled to determine whether the task has been cancelled or not
// 3. We can call the Task.checkCancellation() method.
//    This will throw a CancellationError if the task has been cancelled or do nothing otherwise
// 4. Some parte of Foundation automatically check for task cancellation
// 5. The task will sleep event when cancelled
// 6. If te task is part of a group and any part of the group throws an error, the other task will be cancelled and awaited
// 7. If we have started a task using SwiftUI task() modifier, that task will automatically be canceled when the view disappears

// There is a check for cancelation inside the method URLSession.shared.data(from:)
func getAverageTemperature() async {
    let fetchTask = Task { () -> Double in
        let url = URL(string: "https://hws.dev/readings.json")!
        let (data, _) = try await URLSession.shared.data(from: url)
        let readings = try JSONDecoder().decode([Double].self, from: data)
        let sum = readings.reduce(0, +)
        return sum / Double(readings.count)
    }

    do {
        let result = try await fetchTask.value
        print("Average temperature: \(result)")
      } catch {
        print("Failed to get data.")
    }
}
