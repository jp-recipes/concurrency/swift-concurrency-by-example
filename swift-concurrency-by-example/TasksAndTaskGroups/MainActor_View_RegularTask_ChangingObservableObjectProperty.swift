//
//  Actor_View_DetachedTask_ChangingObservableObjectProperty.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import SwiftUI

class Model: ObservableObject {
    @Published var name = ""
}

struct Actor_View_RegularTask_ChangingObservableObjectProperty: View {
    // "Changes published by observable objects must update the UI on the main thread,
    //  and because any part of the view might try to adjust your object
    //  the only safe approach is for the whole view to run on the main actor"
    //
    // SwiftUI will infers that the whole view must also run on the main actor
    //
    @StateObject private var model = Model()

    var body: some View {
        VStack {
            Text("Hello, \(model.name)")
            Button("Authenticate") {
                Task {
                    model.name = "Taylor"
                }
            }
        }
    }
}

struct Actor_View_DetachedTask_ChangingObservableObjectProperty_Previews: PreviewProvider {
    static var previews: some View {
        Actor_View_RegularTask_ChangingObservableObjectProperty()
    }
}
