//
//  MainActor_View_DetachedTask_ExecuteOutsideMainActor.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import SwiftUI

struct MainActor_View_DetachedTask_ExecuteOutsideMainActor: View {
    @StateObject var model = Model()

    var body: some View {
        Button("Authenticate", action: doWork)
    }

    // Task will be secuentially executed
    func doWork() {
        /*Task.detached {
            for i in 1...100_000 {
                print("In Task 1: \(i)")
            }
        }

        Task.detached {
            for i in 1...100_000 {
                print("In Task 2: \(i)")
            }
        }*/

        usingTaskLocal()
    }
}

struct MainActor_View_DetachedTask_ExecuteOutsideMainActor_Previews: PreviewProvider {
    static var previews: some View {
        MainActor_View_DetachedTask_ExecuteOutsideMainActor()
    }
}
