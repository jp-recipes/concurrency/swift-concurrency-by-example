//
//  Task_Cancel_Check.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

func getAverageTemperatureWithAddedCancellationCheck() async {
    let fetchTask = Task { () -> Double in
        let url = URL(string: "https://hws.dev/readings.json")!
        let (data, _) = try await URLSession.shared.data(from: url)

        try Task.checkCancellation()

        let readings = try JSONDecoder().decode([Double].self, from: data)
        let sum = readings.reduce(0, +)
        return sum / Double(readings.count)
    }

    do {
        let result = try await fetchTask.value
        print("Average temperature: \(result)")
      } catch {
        print("Failed to get data.")
    }
}
