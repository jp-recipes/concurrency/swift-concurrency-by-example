//
//  Task_Yield.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

// With Task.yield() we can voluntary suspend the current task so that Swift can give other task the chance to proceed a little if needed

func factors(for number: Int) async -> [Int] {
    var result = [Int]()

    for check in 1...number {
        if check.isMultiple(of: 100_000) {
            await Task.yield()
        }

        if number.isMultiple(of: check) {
            result.append(check)
            // or better
            // await Task.yield()
        }
    }

    return result
}
