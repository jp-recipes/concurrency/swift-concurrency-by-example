//
//  TaskGroup_Cancel.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

// Swift's task groups can be cancelled in one of three ways
//
// 1. If the parent task of the task group is cancelled
// 2. If we explicitly call cancelAll() on the group
// 3. If one of our child tasks throws an uncaught error, all remaining task wil be implicitlty cancelled
//
// The first of those happens outside of the task group
// cancellAll() will cancel all remaining tasks
//
// As with standalone task, cancelling a task groups is cooperative
// * Our child tasks can check for cancellation using Task.isCancelled or Task.checkCenalltion
//   Our chuld task can ignore cancellation entirely if the want
//
// Remember, calling cancellAll() only cancels remaining tasks, meaning that it won't undo work that has already completed
// Even then the cancellation is cooperative, so we need to make sure the tasks we add to the group check for cancellation

private func printMessage(cancelFirst: Bool) async {
    let result = await withThrowingTaskGroup(of: String.self) { group -> String in
        group.addTask {
            if cancelFirst {
                try Task.checkCancellation()
            }
            return "Testing"
        }
        group.addTask { "Group" }
        group.addTask { "Cancellation" }
        // If cancelFirst is false, the tasks will have finished before cancellation, so we will still have a result of three word string
        group.cancelAll()
        var collected = [String]()

        do {
            for try await value in group {
                collected.append(value)
            }
        } catch {
            print(error.localizedDescription)
        }

        return collected.joined(separator: " ")
    }

    print(result)
}

