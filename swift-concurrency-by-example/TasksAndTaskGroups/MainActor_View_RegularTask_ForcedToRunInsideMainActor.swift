//
//  MainActor_ForcesTaskToRunInMainActor.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import SwiftUI

struct MainActor_View_RegularTask_ForcedToRunInsideMainActor: View {
    var body: some View {
        Button("Authenticate", action: doWork)
    }

    // Task will be secuentially executed
    func doWork() {
        Task {
            for i in 1...1_000 {
                print("In Task 1: \(i)")
            }
        }

        Task {
            for i in 1...1_000 {
                print("In Task 2: \(i)")
            }
        }

        Task {
            for i in 1...1_000 {
                print("In Task 3: \(i)")
            }
        }

        Task {
            for i in 1...1_000 {
                print("In Task 4: \(i)")
            }
        }
    }
}

struct MainActor_ForcesTaskToRunInMainActor_Previews: PreviewProvider {
    static var previews: some View {
        MainActor_View_RegularTask_ForcedToRunInsideMainActor()
    }
}
