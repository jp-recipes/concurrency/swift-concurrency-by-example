//
//  Task_Priority.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

// Priorities
//
// .high, .background, or nil
//
// Priorities are suggestions not enforcements

func fetchQuotestWithHighPriority() async {
    let downloadTask = Task(priority: .high) { () -> String in
        let url = URL(string: "https://hws.dev/chapter.txt")!
        let (data, _) = try await URLSession.shared.data(from: url)
        return String(decoding: data, as: UTF8.self)
    }

    do {
        let text = try await downloadTask.value
        print(text)
    } catch {
        print(error.localizedDescription)
    }
}

func initilizingTaskFromMainThreadWillAssignPriorityAsUserInitiated() async {
    Task {
        print("Task.currentPriority: \(Task.currentPriority == .userInitiated)")
    }
}
