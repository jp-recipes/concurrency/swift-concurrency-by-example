//
//  TaskLocal_LogLevel.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import Foundation

// In real-world code, task-local values are useful for places where you need to repeatedly pass values around inside your taks - values that need to be shared within the task, but not across your whole program lika a singleton might be

enum LogLevel: Comparable {
    case debug, info, warn, error, fatal
}

struct Logger {
    @TaskLocal static var logLevel = LogLevel.info

    private init() { }

    static let shared = Logger()

    func write(_ message: String, level: LogLevel) {
        if level >= Logger.logLevel {
            print(message)
        }
    }
}

struct MainApp {
    static func fetch(url urlString: String) async throws -> String? {
        Logger.shared.write("Preparing request: \(urlString)", level: .debug)

        if let url = URL(string: urlString) {
            let (data, _) = try await URLSession.shared.data(from: url)
            Logger.shared.write("Received \(data.count) bytes", level: .info)
            return String(decoding: data, as: UTF8.self)
        } else {
            Logger.shared.write("URL \(urlString) is invalid", level: .error)
            return nil
        }
    }

    static func main() async throws {
        Task {
            try await Logger.$logLevel.withValue(.debug) {
                try await fetch(url: "https://hws.dev/news-1.json")
            }
        }

        Task {
              try await Logger.$logLevel.withValue(.error) {
                try await fetch(url: "https:\\hws.dev/news-1.json")
              }
        }
    }
}
