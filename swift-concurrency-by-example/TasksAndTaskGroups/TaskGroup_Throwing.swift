//
//  TaskGroup_Throwing.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

struct NewsStory: Identifiable, Decodable {
    let id: Int
    let title: String
    let strap: String
    let url: URL
}

// There are four things there that deserve special attention
//
// 1. Fetching and deconding items might throw errors, and those error are NOT handled inside tasks, so we need to use withThroingTaskGroup() to create a group
// 2. One od the main advantages of task groups is being able to add task inside a loop
// 3. Because the task group conforms to AsyncSequence, we can call its `reduce()` method to boil all its task results down to a single value
// 4. Tasks in a group can complete in any order, so we sorted the resulting array of news stories to get them all in a sensible order

func loadSortedStories() async throws -> [NewsStory] {
    return try await withThrowingTaskGroup(of: [NewsStory].self) { group in
        for i in 1...5 {
            group.addTask {
                let url = URL(string: "https://hws.dev/news-\(i).json")!
                let (data, _) = try await URLSession.shared.data(from: url)
                return try JSONDecoder().decode([NewsStory].self, from: data)
            }
        }

        let allStories = try await group.reduce(into: []) { $0 += $1 }
        return allStories.sorted { $0.id > $1.id }
    }
}
