//
//  TaskGroup_Throwing_View.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import SwiftUI

struct TaskGroup_Throwing_View: View {
    @State private var stories = [NewsStory]()

    var body: some View {
        NavigationView {
            List(stories) { story in
                VStack(alignment: .leading) {
                    Text(story.title)
                        .font(.headline)

                    Text(story.strap)
                }
            }
            .navigationTitle("Latest News")
        }
        .task {
            await loadStories()
        }
    }

    private func loadStories() async {
        do {
            stories = try await loadSortedStories()
        } catch {
            print("Failed to load stories")
        }
    }
}

struct TaskGroup_Throwing_View_Previews: PreviewProvider {
    static var previews: some View {
        TaskGroup_Throwing_View()
    }
}
