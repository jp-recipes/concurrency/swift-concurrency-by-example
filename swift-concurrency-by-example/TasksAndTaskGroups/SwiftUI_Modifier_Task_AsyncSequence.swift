//
//  SwiftUI_Modifier_Task_AsyncSequence.swift
//  swift-concurrency-by-example
//
//  Created by JP on 30-08-23.
//

import SwiftUI

struct NumberGenerator: AsyncSequence, AsyncIteratorProtocol {
    typealias Element = Int

    let delay: Double
    let range: ClosedRange<Int>

    init(range: ClosedRange<Int>, delay: Double = 10) {
        self.range = range
        self.delay = delay
    }

    mutating func next() async -> Int? {
        while !Task.isCancelled {
            try? await Task.sleep(nanoseconds: UInt64(delay) * 1_000_000_000)
            print("Generating number")
            return Int.random(in: range)
        }

        return nil
    }

    func makeAsyncIterator() -> NumberGenerator {
        self
    }
}

struct SwiftUI_Modifier_Task_AsyncSequence: View {
    var body: some View {
        NavigationView {
            NavigationLink(destination: DetailView()) {
                Text("Start Generating Numbers")
            }
        }
    }
}

struct SwiftUI_Modifier_Task_AsyncSequence_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUI_Modifier_Task_AsyncSequence()
    }
}

// This generates and siplays all the random numbers we've generated
struct DetailView: View {
    @State private var numbers = [String]()
    let generator = NumberGenerator(range: 1...100)

    var body: some View {
        List(numbers, id: \.self, rowContent: Text.init)
            .task {
                await generateNumbers()
            }
    }

    private func generateNumbers() async {
        for await number in generator {
            numbers.insert("\(numbers.count + 1). \(number)", at: 0)
        }
    }
}
