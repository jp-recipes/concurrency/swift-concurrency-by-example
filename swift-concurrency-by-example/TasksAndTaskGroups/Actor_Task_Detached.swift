//
//  Actor_Task_Detached.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

actor DetaichedTaskUserActor {
    func detachedLoginTask() {
        Task.detached {
            // await is required for sycnhronous calls
            if await self.authenticate(user: "j", password: "123") {
                print("Successfully logged in.")
            } else {
                print("Sorry, something went wrong.")
            }
        }
    }

    func authenticate(user: String, password: String) -> Bool {
        return true
    }
}

func callDetachedUserTask() async {
    let user = DetaichedTaskUserActor()
    await user.detachedLoginTask()
}
