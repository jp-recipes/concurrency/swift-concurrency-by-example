//
//  TaskGroup_TaskThrows_Uncaught.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

// Just throwing and error inside addTask() isn't enough to cause other tasks in the group to be cancelled
// This only happens when you access the value of the throwing task using next() or when looping over the child tasks

enum ExampleError: Error {
    case badURL
}

func testCancellation() async {
    do {
        try await withThrowingTaskGroup(of: Void.self) { group -> Void in
            group.addTask {
                try await Task.sleep(nanoseconds: 1_000_000_000)
                throw ExampleError.badURL
            }

            group.addTask {
                try await Task.sleep(nanoseconds: 2_000_000_000)
                print("Task is cancelled: \(Task.isCancelled)")
            }

            try await group.next()
        }
    } catch {
        print("Error thrown: \(error.localizedDescription)")
    }
}
