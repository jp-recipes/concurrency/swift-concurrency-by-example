//
//  Tasks_MessageView.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import SwiftUI

// 1. Creating a new task is what allows to start calling an async function even though the button's action is a synchronous function
// 2. The lifetime of the task is no bound to the button's action closure
//    So, even tough the closure will finish immediately, the task it created will carry on running to completion
// 3. We aren't trying to read a value from the task, or storing it enywhere
//    This task doesn't actually return anything, and doesn't need to

// * All UI work runs on the main thread
// * The task will execute on a background thread
// * Inside loadMessage() we use wait to load our URL data, and when that resumes we have another potential thread switch
// * Finnaly, the messages property uses the @State property wrapper, which will automatically update its value on the main thread

struct Task_MessageView: View {
    @State private var messages = [Message]()

    var body: some View {
        NavigationView {
            Group {
                if messages.isEmpty {
                    Button("Load Messages") {
                        Task {
                            await loadMessages()
                        }
                    }
                } else {
                    List(messages) { message in
                        VStack(alignment: .leading) {
                            Text(message.from)
                                .font(.headline)

                            Text(message.text)
                        }
                    }
                }
            }
            .navigationTitle("Inbox")
        }
    }

    private func loadMessages() async {
        do {
            let url = URL(string: "https://hws.dev/messages.json")!
            let (data, _) = try await URLSession.shared.data(from: url)

            messages = try JSONDecoder().decode([Message].self, from: data)
        } catch {
            messages = [
                Message(id: 0, from: "Failed to load inbox.", text: "Please try again later.")
            ]
        }
    }
}

struct Task_MessageView_Previews: PreviewProvider {
    static var previews: some View {
        Task_MessageView()
    }
}
