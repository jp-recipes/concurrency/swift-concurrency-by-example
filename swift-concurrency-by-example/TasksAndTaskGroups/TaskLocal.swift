//
//  TaskLocal.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

// Task-local values are analogous to thread-local values in an old-style multithreading environment
//
// We attach some metadata to our task, and any code running iside that task can read that data as needed

enum Coder {
    @TaskLocal static var id = "Anonymous"
}

func usingTaskLocal() {
    Task {
        try await Coder.$id.withValue("Jota") {
            print("Start of task: \(Coder.id)")
            try await Task.sleep(nanoseconds: 1_000_000)
            print("End of task: \(Coder.id)")
        }
    }

    Task {
        try await Coder.$id.withValue("Pro") {
            print("Start of task: \(Coder.id)")
            try await Task.sleep(nanoseconds: 1_000_000)
            print("End of task: \(Coder.id)")
        }
    }

    print("Outside of tasks: \(Coder.id)")
}
