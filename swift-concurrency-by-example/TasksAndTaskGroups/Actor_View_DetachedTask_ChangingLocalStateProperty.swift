//
//  Actor_View_DetachedTask_ChangingLocalStateProperty.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import SwiftUI

struct Actor_View_DetachedTask_ChangingLocalStateProperty: View {
    // @State garantees it's safe to change its value on any thread
    //
    // We can usa a Detached Task instead enven though it won't inherit actor isolation
    @State private var name = "Anonymous"

    var body: some View {
        VStack {
            Text("Hello, \(name)!")
            Button("Authenticate") {
                Task.detached {
                    name = "Taylor"
                }
            }
        }
    }
}

struct Actor_View_DetachedTask_ChangingLocalStateProperty_Previews: PreviewProvider {
    static var previews: some View {
        Actor_View_DetachedTask_ChangingLocalStateProperty()
    }
}
