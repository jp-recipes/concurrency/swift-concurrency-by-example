//
//  TaskGroup.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

// Task Groups are collections of task that work together to produce a signle result
// Each task inside a group must return the same kind of data, but if we use enum associated values we can make them send back different kinds of data - it's a little clumsy, but it works
//
// 1. We must specify the exact type of data our task group will return
// 2. We needto to specify exactly what the return value of the group will be
// 3. We call addTask() once for each task we want to add to the group
// 4. Task groups conform AsyncSequence, so we can read all the values from their children using for await, o by calling group.next() repeatdly
// 5. Because the whole task group executes asynchronously, we must call it using await

private func printMessage() async {
    let string = await withTaskGroup(of: String.self) { group in
        group.addTask { "Hello" }
        group.addTask { "From" }
        group.addTask { "A" }
        group.addTask { "Task" }
        group.addTask { "Group" }

        var collected = [String]()

        for await value in group {
            collected.append(value)
        }

        return collected.joined(separator: " ")
    }

    print(string)
}
