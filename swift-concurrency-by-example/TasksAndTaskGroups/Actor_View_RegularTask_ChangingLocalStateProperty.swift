//
//  Actor_View_RegularTaskChangingPropertyState.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import SwiftUI

struct Actor_View_RegularTask_ChangingLocalStateProperty: View {
    @State private var name = "Anonymous"

    var body: some View {
        VStack {
            Text("Hello, \(name)!")
            Button("Authenticate") {
                // Task is not needed because we are setting a local value
                // We are just trying to illustrate how regular tasks and detached tasks are different
                Task {
                    name = "Taylor"
                }
            }
        }
    }
}

struct Actor_View_RegularTaskChangingPropertyState_Previews: PreviewProvider {
    static var previews: some View {
        Actor_View_RegularTask_ChangingLocalStateProperty()
    }
}
