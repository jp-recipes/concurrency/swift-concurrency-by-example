//
//  Task_Regular_View.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

actor RegularTaskUserActor {
    func regularLoginTask() {
        Task {
            // Synchronous call (no await required)
            if authenticate(user: "j", password: "123") {
                print("Successfully logged in.")
            } else {
                print("Sorry, something went wrong.")
            }
        }
    }

    func authenticate(user: String, password: String) -> Bool {
        return true
    }
}

func callUserActorLogin() async {
    let user = RegularTaskUserActor()
    await user.regularLoginTask()
}
