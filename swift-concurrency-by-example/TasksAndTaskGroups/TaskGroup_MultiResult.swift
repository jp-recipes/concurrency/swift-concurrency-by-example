//
//  TaskGroup_MultiResult.swift
//  swift-concurrency-by-example
//
//  Created by JP on 29-08-23.
//

import Foundation

struct Letter: Decodable {
    let id: Int
    let from: String
    let message: String
}

struct Person {
    let username: String
    let favorites: Set<Int>
    let letters: [Letter]
}

enum FetchResult {
    case username(String)
    case favorites(Set<Int>)
    case letters([Letter])
}


func loadUser() async {
    let person = await withThrowingTaskGroup(of: FetchResult.self) { group -> Person in
        group.addTask {
            let url = URL(string: "https://hws.dev/username.json")!
            let (data, _) = try await URLSession.shared.data(from: url)
            let result = String(decoding: data, as: UTF8.self)

            return .username(result)
        }

        group.addTask {
            let url = URL(string: "https://hws.dev/user-favorites.json")!
            let (data, _) = try await URLSession.shared.data(from: url)
            let result = try JSONDecoder().decode(Set<Int>.self, from: data)

            return .favorites(result)
        }

        group.addTask {
            let url = URL(string: "https://hws.dev/user-messages.json")!
            let (data, _) = try await URLSession.shared.data(from: url)
            let result = try JSONDecoder().decode([Letter].self, from: data)

            return .letters(result)
        }

        var username = "Anonymous"
        var favorites = Set<Int>()
        var letters = [Letter]()

        do {
            for try await value in group {
                switch value {
                case .username(let value):
                    username = value
                case .favorites(let value):
                    favorites = value
                case .letters(let value):
                    letters = value
                }
            }
        } catch {
            print("Fetch at least partially failed; sending back what we have so far. \(error.localizedDescription)")
        }

        return Person(username: username, favorites: favorites, letters: letters)
    }

    print("Person \(person.username) has \(person.letters.count) messages and \(person.favorites.count) favorites")
}
