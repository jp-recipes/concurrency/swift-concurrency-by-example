//
//  swift_concurrency_by_exampleApp.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import SwiftUI

@main
struct swift_concurrency_by_exampleApp: App {
    var body: some Scene {
        WindowGroup {
            MainActor_View_DetachedTask_ExecuteOutsideMainActor()
        }
    }
}
