//
//  Parallel_Execution_with_Async_Let.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import Foundation

// async let captures any value it uses
func loadData() async {
    async let (userData, _) = URLSession.shared
        .data(from: URL(string: "https://hws.dev/user-24601.json")!)

    async let (messageData, _) = URLSession.shared
        .data(from: URL(string: "https://hws.dev/user-messages.json")!)

    do {
        let decoder = JSONDecoder()
        let user = try await decoder.decode(User.self, from: userData)
        let messages = try await decoder.decode([Message].self, from: messageData)

        print("User \(user.name) has \(messages.count) message(s).")
    } catch {
        print("Sorry, there was a network problem.")
    }
}

func fetchFavorites(for user: User) async -> [Int] {
    print("Fetching favorites for \(user.name)...")

    do {
        async let (favorites, _) = URLSession.shared
            .data(from: URL(string: "https://hws.dev/user-favorites.json")!)

        return try await JSONDecoder().decode([Int].self, from: favorites)
    } catch {
        return []
    }
}


func callFavorites() async {
    // Swift won't let us capture a var, it must be a constant (so it won't change)
    // var user = User(id: UUID(), name: "Taylor Swift", age: 26)
    let user = User(id: UUID(), name: "Taylor Swift", age: 26)
    async let favorites = fetchFavorites(for: user)
    await print("Found \(favorites.count) favorites")
}

struct News {}
struct Weather {}

func getNews() async -> [News] { return [] }
func getWeather() async -> [Weather] { return [] }
func getAppUpdateAvailable() async -> Bool { false }

func concurrentUnrelatedAsyncOperations() async -> ([News], [Weather], Bool) {
    async let news = getNews()
    async let weather = getWeather()
    async let hasUpdate = getAppUpdateAvailable()
    return await (news, weather, hasUpdate)
}
