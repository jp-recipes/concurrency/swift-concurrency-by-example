//
//  FetchAppleDataView.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import SwiftUI

struct AsyncAwait_View_Task_Modifier: View {
    @State private var site = "https://"
    @State private var sourceCode = ""

    var body: some View {
        ScrollView {
            Text(sourceCode)
        }
        .task {
            await fetchSource()
        }
    }

    func fetchSource() async {
        do {
            let url = URL(string: "https://apple.com")!

            let (data, _) = try await URLSession.shared.data(from: url)

            sourceCode = String(decoding: data, as: UTF8.self)
                .trimmingCharacters(in: .whitespacesAndNewlines)
        } catch {
            sourceCode = "Failed to fetch apple.com"
        }
    }
}

struct FetchAppleDataView_Previews: PreviewProvider {
    static var previews: some View {
        AsyncAwait_View_Task_Modifier()
    }
}
