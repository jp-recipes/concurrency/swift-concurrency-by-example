//
//  RemoteFile_Async_Computed_Properties.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import SwiftUI

struct User: Identifiable, Decodable {
    var id = UUID()
    let name: String
    let age: Int
}

struct Message: Decodable, Identifiable {
  let id: Int
  let from: String
  let text: String
}

struct RemoteFile<T: Decodable> {
    let url: URL
    let type: T.Type

    var contents: T {
        get async throws {
            let (data, _) = try await URLSession.shared.data(from: url)
            return try JSONDecoder().decode(T.self, from: data)
        }
    }
}

struct RemoteFileView: View {
    let source = RemoteFile(
        url: URL(string: "https://hws.dev/inbox.json")!,
        type: [Message].self
    )

    @State private var messages: [Message] = []

    var body: some View {
        NavigationView {
            List(messages) { message in
                VStack(alignment: .leading) {
                    Text(message.from)
                        .font(.headline)
                    Text(message.text)
                }
            }
            .navigationTitle("Inbox")
            .toolbar {
                Button(action: refresh) {
                    Label("Refresh", systemImage: "arrow.clockwise")
                }
            }
            .onAppear(perform: refresh)
        }
    }

    private func refresh() {
        Task {
            do {
                // Access the property asynchronously
                messages = try await source.contents
            } catch {
                print("Message update failed.")
            }
        }
    }
}

struct RemoteFileView_Previews: PreviewProvider {
    static var previews: some View {
        RemoteFileView()
    }
}
