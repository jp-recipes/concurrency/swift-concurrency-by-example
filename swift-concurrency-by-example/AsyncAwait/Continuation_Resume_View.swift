//
//  Continuation_Resume_View.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import SwiftUI
import CoreLocationUI

struct Continuation_Resume_View: View {
    @StateObject private var locationManager = LocationManager()

    var body: some View {
        LocationButton {
            Task {
                if let location = try? await locationManager.requestLocation() {
                    print("Location: \(location)")
                } else {
                    print("Location unknown.")
                }
            }
        }
        .frame(height: 44)
        .foregroundColor(.white)
        .clipShape(Capsule())
        .padding()
    }
}

struct Continuation_Resume_View_Previews: PreviewProvider {
    static var previews: some View {
        Continuation_Resume_View()
    }
}
