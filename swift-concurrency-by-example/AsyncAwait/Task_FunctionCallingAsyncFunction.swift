//
//  Task_FunctionCallingAsyncFunction.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import Foundation

func doAsyncWork() async {
    print("Doing async work")
}

func doRegularWork() {
    // Task will be run immediately. We aren't waiting fot the task to complete
    // So, we shouldn't use `await` when creating it
    Task {
        await doAsyncWork()
    }
}
