//
//  Continuation.swift
//  swift-concurrency-by-example
//
//  Created by JP on 28-08-23.
//

import Foundation

struct Inbox: Decodable, Identifiable {
    let id: Int
    let user: String
    let text: String
}

func fetchMessages(completion: @escaping ([Inbox]) -> Void) {
    let url = URL(string: "https://hws.dev/user-messages.json")!

    URLSession.shared.dataTask(with: url) { data, response, error in
        if let data = data {
            if let messages = try? JSONDecoder().decode([Inbox].self, from: data) {
                completion(messages)
                return
            }
        }

        completion([])
    }.resume()
}

func fetchMessages() async -> [Inbox] {
    // It's called `checked` because:
    // The continuation must be resumed exactly once
    // Not zero times, and not twice or more times - exactly once
    //
    // It will crash if it's called more then once
    // Continuation must be used or be warned
    //
    // Alternatively use `unchecked` version `withUnsafeContinuation()`
    await withCheckedContinuation { continuation in
        fetchMessages { messages in
            continuation.resume(returning: messages)
        }
    }
}

func callFetchMessages() async {
    let messages = await fetchMessages()
    print("Downlaoded \(messages.count) messages.")
}
